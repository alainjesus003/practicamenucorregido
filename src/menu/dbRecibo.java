package menu;
import java.sql.*;
public class dbRecibo {
    
    private String MYSQLDRIVER = "com.mysql.cj.jdbc.Driver";
    private String MYSQLDB = "jdbc:mysql://3.132.136.208:3306/jesush?user=jesush&password=C0ntras3n1a";
    private Connection conexion;
    private String strConsulta;
    private ResultSet registros;
    
    
    public dbRecibo(){
     try{
        Class.forName(MYSQLDRIVER);
     }catch(ClassNotFoundException e){
         System.out.println("Surgio un error " + e.getMessage());
         System.exit(-1);
     }
    }
    
    public void conectar(){
        try{
            conexion = DriverManager.getConnection(MYSQLDB);
        }catch (SQLException e){
            System.out.println("No se logro conectar " +e.getMessage());
        }
    }
    
    public void desconectar(){
        try{
            conexion.close();
        }catch (SQLException e){
            System.out.println("Surgio un error al desconectar" +e.getMessage());
        }
    }
    
    public void insertar(Recibo rec){
        conectar();
        try{
            strConsulta = "INSERT INTO recibo(numRecibo,fecha,nombre,domicilio,tipo,costo,consumo,status)"+"VALUES(?,CURDATE(),?,?,?,?,?,?)";
            
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, rec.getNumRecibo());
            pst.setString(2, rec.getNombre());
            pst.setString(3, rec.getDomicilio());
            pst.setInt(4, rec.getTipo());
            pst.setFloat(5, rec.getCosto());
            pst.setFloat(6, rec.getConsumo());
            pst.setInt(7, rec.getStatus());
            
            pst.executeUpdate();
        }catch (SQLException e){
            System.out.println("Error al insertar" +e.getMessage());
        }
        desconectar();
    }
    
    public void actualizar(Recibo rec){
        Recibo recibo = new Recibo();
        
        strConsulta = "UPDATE recibo SET nombre = ?, domicilio = ?, fecha = CURDATE(), tipo = "
                +"? , costo = ? ,consumo = ? WHERE numRecibo = ? and status = 0;";
        this.conectar();
        try{
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
           
            pst.setString(1, rec.getNombre());
            pst.setString(2, rec.getDomicilio());
           // pst.setString(3, rec.getFecha());
            pst.setInt(3, rec.getTipo());
            pst.setFloat(4, rec.getCosto());
            pst.setFloat(5, rec.getConsumo());
            pst.setInt(6, rec.getNumRecibo());
            
            
            pst.executeUpdate();
            this.desconectar();
        }catch(SQLException e){
            System.err.println("Surgio un error al actualizar: "+e.getMessage());
        }
    }
    
    public void habilitar(Recibo rec){
        
        String consulta = "";
        strConsulta = "UPDATE recibo SET status = 0 WHERE numRecibo = ?";
        this.conectar();
        try{
            System.err.println("Se conecto");
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            
            pst.setInt(1, rec.getNumRecibo());
            
            pst.executeUpdate();
            this.desconectar();
        }catch(SQLException e){
            System.err.println("Surgio un error al habilitar" +e.getMessage());
        }
    }
    
    public void deshabilitar(Recibo rec){
        strConsulta = "UPDATE recibo SET status = 1 WHERE numRecibo = ?";
        this.conectar();
        try{
            System.err.println("Se conecto");
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            
            pst.setInt(1, rec.getNumRecibo());
            
            pst.executeUpdate();
            this.desconectar();
        }catch(SQLException e){
            System.err.println("Surgio un error al habilitar "+e.getMessage());
        }  
    }
    
    public boolean isExiste(int numRecibo, int status){
        boolean exito=false;
        this.conectar();
        strConsulta="SELECT * FROM recibo WHERE numRecibo = ? and status = ?;";
        try{
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, numRecibo);
            pst.setInt(2, status);
            this.registros = pst.executeQuery();
            if(this.registros.next()) exito=true;
        }catch(SQLException e){
            System.err.println("Surgio un error al verificar si existe: "+e.getMessage());
        }
        this.desconectar();
        return exito;
    }
    
    public Recibo buscar(int numRecibo){
        Recibo recibo=new Recibo();
        conectar();
        try{
            strConsulta= "SELECT * FROM recibo WHERE numRecibo = ? and status = 0;";
            PreparedStatement pst=conexion.prepareStatement(strConsulta);
            
            pst.setInt(1, numRecibo);
            this.registros=pst.executeQuery();
            if(this.registros.next()){
                recibo.setId(registros.getInt("id"));
                recibo.setNumRecibo(registros.getInt("numRecibo"));
                recibo.setNombre(registros.getString("nombre"));
                recibo.setDomicilio(registros.getString("domicilio"));
                recibo.setTipo(registros.getInt("tipo"));
                recibo.setCosto(registros.getFloat("costo"));
                recibo.setConsumo(registros.getFloat("consumo"));
                recibo.setFecha(registros.getString("fecha"));
            }else recibo.setId(0);
        }catch(SQLException e){
            System.err.println("Surgio un error al habilitar: "+e.getMessage());
        }
        this.desconectar();
        return recibo;
    }
    
   
}