
package menu;

import java.awt.Dimension;
import javax.swing.JOptionPane;
/**
 *
 * @author Alain
 */
public class jdlgMenu extends javax.swing.JDialog {

    /**
     * Creates new form jdlgMenu
     */
    public jdlgMenu(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setSize(1000,1000);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jdp = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jmiTerreno = new javax.swing.JMenuItem();
        jmiRecibo = new javax.swing.JMenuItem();
        jmiCotizacion = new javax.swing.JMenuItem();
        jmiSalir = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        javax.swing.GroupLayout jdpLayout = new javax.swing.GroupLayout(jdp);
        jdp.setLayout(jdpLayout);
        jdpLayout.setHorizontalGroup(
            jdpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jdpLayout.setVerticalGroup(
            jdpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 277, Short.MAX_VALUE)
        );

        jMenu1.setText("MisProgramas");

        jmiTerreno.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jmiTerreno.setText("Terreno");
        jmiTerreno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiTerrenoActionPerformed(evt);
            }
        });
        jMenu1.add(jmiTerreno);

        jmiRecibo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jmiRecibo.setText("Recibo");
        jmiRecibo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiReciboActionPerformed(evt);
            }
        });
        jMenu1.add(jmiRecibo);

        jmiCotizacion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jmiCotizacion.setText("Cotización");
        jmiCotizacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiCotizacionActionPerformed(evt);
            }
        });
        jMenu1.add(jmiCotizacion);

        jmiSalir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jmiSalir.setText("Salir");
        jmiSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSalirActionPerformed(evt);
            }
        });
        jMenu1.add(jmiSalir);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jdp)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jdp)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jmiTerrenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiTerrenoActionPerformed
        // TODO add your handling code here:
        jifTerreno ter = new jifTerreno();
        this.jdp.add(ter);

        Dimension pantallaSize = jdp.getSize();
        Dimension FrameSize = ter.getSize();

        ter.setLocation ((pantallaSize.width - FrameSize.width)/2,(pantallaSize.height - FrameSize.height)/2);

        ter.show();
    }//GEN-LAST:event_jmiTerrenoActionPerformed

    private void jmiReciboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiReciboActionPerformed
        // TODO add your handling code here:
        jifRecibo re = new jifRecibo();
        this.jdp.add(re);

        Dimension pantallaSize = jdp.getSize();
        Dimension FrameSize = re.getSize();

        re.setLocation ((pantallaSize.width - FrameSize.width)/2,(pantallaSize.height - FrameSize.height)/2);

        re.show();
    }//GEN-LAST:event_jmiReciboActionPerformed

    private void jmiCotizacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiCotizacionActionPerformed
        // TODO add your handling code here:
        jifCotizacion co = new jifCotizacion();
        this.jdp.add(co);

        Dimension pantallaSize = jdp.getSize();
        Dimension FrameSize = co.getSize();

        co.setLocation ((pantallaSize.width - FrameSize.width)/2,(pantallaSize.height - FrameSize.height)/2);

        co.show();
    }//GEN-LAST:event_jmiCotizacionActionPerformed

    private void jmiSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSalirActionPerformed
        // TODO add your handling code here:
        int res = JOptionPane.showConfirmDialog(this,"Desea Salir?","Menu",JOptionPane.YES_NO_OPTION);
        this.setSize(600, 600);
        if (res == JOptionPane.YES_OPTION) this.dispose();
    }//GEN-LAST:event_jmiSalirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jdlgMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jdlgMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jdlgMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jdlgMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jdlgMenu dialog = new jdlgMenu(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JDesktopPane jdp;
    private javax.swing.JMenuItem jmiCotizacion;
    private javax.swing.JMenuItem jmiRecibo;
    private javax.swing.JMenuItem jmiSalir;
    private javax.swing.JMenuItem jmiTerreno;
    // End of variables declaration//GEN-END:variables

}
