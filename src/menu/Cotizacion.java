
package menu;

public class Cotizacion {
    
    private int numCotizacion;
    private String descripcion;
    private float precio;
    private float porcentaje;
    private int plazo;

  
    public Cotizacion() {
        this.numCotizacion = 0;
        this.descripcion = "";
        this.precio = 0.0f;
        this.porcentaje = 0.0f;
        this.plazo = 0;
    }

    public Cotizacion(int numCotizacion, String descripcion, float precio, float porcentaje, int plazo) {
        this.numCotizacion = numCotizacion;
        this.descripcion = descripcion;
        this.precio = precio;
        this.porcentaje = porcentaje;
        this.plazo = plazo;
    }
    
    public Cotizacion(Cotizacion otro) {
        this.numCotizacion =otro.numCotizacion;
        this.descripcion =otro.descripcion;
        this.precio =otro.precio;
        this.porcentaje =otro.porcentaje;
        this.plazo =otro.plazo;
    }
    
    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(float porcentaje) {
        this.porcentaje = porcentaje;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    //metodos de comportamiento
    public float calcularPagoInicial() {
        float inicial = 0.0f;
        inicial = precio * (porcentaje / 100);
        return inicial;
    }

    public float calcularTotalFinanciar() {
        float total = 0.0f;
        total = precio - calcularPagoInicial();
        return total;
    }

    public float calcularPagoMensual() {
        float mensual = 0.0f;
        mensual = calcularTotalFinanciar() / plazo;
        return mensual;
    }     
}
