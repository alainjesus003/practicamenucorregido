package menu;


public class Recibo {
     //atributos de la clase
    private int id;
    private int numRecibo;
    private String fecha;
    private String nombre;
    private String domicilio;
    private int tipo;
    private float costo;
    private float consumo;
    private int status;
    //constructores
    public Recibo(){
        this.id=0;
        this.numRecibo=0;
        this.fecha="";
        this.nombre="";
        this.domicilio="";
        this.tipo=0;
        this.costo=0.0f;
        this.consumo=0.0f;
        this.status=0;
    }

    public Recibo(int id, int numRecibo, String fecha, String nombre, String domicilio, int tipo, float costo, float consumo, int status) {
        this.id = id;
        this.numRecibo = numRecibo;
        this.fecha = fecha;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.tipo = tipo;
        this.costo = costo;
        this.consumo = consumo;
        this.status = status;
    }
    
    public Recibo(Recibo otro) {
        this.id = otro.id;
        this.numRecibo = otro.numRecibo;
        this.fecha = otro.fecha;
        this.nombre = otro.nombre;
        this.domicilio = otro.domicilio;
        this.tipo = otro.tipo;
        this.costo = otro.costo;
        this.consumo = otro.consumo;
        this.status = otro.status;
    }
    
    
    //metodos
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {    
        this.status = status;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public float getConsumo() {
        return consumo;
    }

    public void setConsumo(float consumo) {
        this.consumo = consumo;
    }
    
    //metodos de comportamiento
    public float calcularSubtotal(){
        float Subtotal=0.0f;
        switch(this.tipo){
            case 1:
                this.costo=2.00f;
                Subtotal=this.consumo*this.costo;
                break;
            case 2:
                this.costo=3.00f;
                Subtotal=this.consumo*this.costo;
                break;
            case 3:
                this.costo=5.00f;
                Subtotal=this.consumo*this.costo;
                break;
            default:
                System.out.println("Opcion no valida");
        }
        return Subtotal;
    }
    
    public float calcularImpuesto(){
    float Impuesto=0.0f;
    Impuesto=this.calcularSubtotal()*0.16f;
    return Impuesto;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularSubtotal()+this.calcularImpuesto();
    return total;
    }
    
}
